package com.dmestro.transportforecast.service;


public interface StationService {
    String NAME = "transportforecast_StationService";
    void loadStations();
}