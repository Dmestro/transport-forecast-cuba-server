package com.dmestro.transportforecast.core;

import com.dmestro.transportforecast.entity.Station;
import com.dmestro.transportforecast.tool.ToSamaraHelper;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Transaction;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.security.app.Authentication;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.util.List;
import java.util.UUID;

@Component("transport_StationsMBean")
public class Stations implements StationsMBean {
    @Inject
    protected Persistence persistence;

    @Inject
    private Metadata metadata;

    @Inject
    Authentication authentication;


    @Inject
    private ToSamaraHelper toSamaraHelper;

    @Override
    public void loadStations() {


        authentication.begin();
        try {
                List<Station> stations = toSamaraHelper.getStations();
                for (Station station: stations) {
                    try(Transaction transaction = persistence.createTransaction()){
                    persistence.getEntityManager().persist(station);
                    transaction.commit();
                    }
                }
        } finally {
            authentication.end();
        }

    }
}
