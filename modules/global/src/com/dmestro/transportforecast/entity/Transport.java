package com.dmestro.transportforecast.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import com.haulmont.cuba.core.entity.annotation.Lookup;
import com.haulmont.cuba.core.entity.annotation.LookupType;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.haulmont.cuba.core.entity.StandardEntity;

@Table(name = "TRANSPORTFORECAST_TRANSPORT")
@Entity(name = "transportforecast$Transport")
public class Transport extends StandardEntity {
    private static final long serialVersionUID = 4297580527717292630L;

    @Column(name = "KR_ID", nullable = false)
    protected Integer krId;

    @Column(name = "NUMBER_", nullable = false, length = 10)
    protected String number;

    @Column(name = "TYPE_")
    protected String type;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "ARRIVAL_TIME", nullable = false)
    protected Date arrivalTime;

    @Lookup(type = LookupType.DROPDOWN, actions = {"lookup", "open"})
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "STATION_ID")
    protected Station station;

    @Column(name = "TIME_TO_STOP")
    protected Integer timeToStop;


    @Column(name = "REMAINING_LENGTH")
    protected Double remainingLength;

    public void setRemainingLength(Double remainingLength) {
        this.remainingLength = remainingLength;
    }

    public Double getRemainingLength() {
        return remainingLength;
    }


    public void setTimeToStop(Integer timeToStop) {
        this.timeToStop = timeToStop;
    }

    public Integer getTimeToStop() {
        return timeToStop;
    }


    public void setKrId(Integer krId) {
        this.krId = krId;
    }

    public Integer getKrId() {
        return krId;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getNumber() {
        return number;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getType() {
        return type;
    }

    public void setArrivalTime(Date arrivalTime) {
        this.arrivalTime = arrivalTime;
    }

    public Date getArrivalTime() {
        return arrivalTime;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public Station getStation() {
        return station;
    }


}