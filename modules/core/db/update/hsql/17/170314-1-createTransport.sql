create table TRANSPORTFORECAST_TRANSPORT (
    ID varchar(36) not null,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    KR_ID integer not null,
    NUMBER_ varchar(10) not null,
    TYPE_ varchar(255),
    ARRIVAL_TIME timestamp not null,
    STATION_ID varchar(36) not null,
    --
    primary key (ID)
);
