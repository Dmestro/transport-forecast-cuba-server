package com.dmestro.transportforecast.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.chile.core.annotations.NamePattern;

@NamePattern("%s %s %s %s|id,creationTime,rating,text")
@Table(name = "TRANSPORTFORECAST_MESSAGE")
@Entity(name = "transportforecast$Message")
public class Message extends StandardEntity {
    private static final long serialVersionUID = -2834517956504266962L;

    @Column(name = "TEXT", nullable = false)
    protected String text;

    @Column(name = "RATING", nullable = false)
    protected Integer rating;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(name = "TRANSPORT_ID")
    protected Transport transport;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "CREATION_TIME", nullable = false)
    protected Date creationTime;

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    public Integer getRating() {
        return rating;
    }

    public void setTransport(Transport transport) {
        this.transport = transport;
    }

    public Transport getTransport() {
        return transport;
    }

    public void setCreationTime(Date creationTime) {
        this.creationTime = creationTime;
    }

    public Date getCreationTime() {
        return creationTime;
    }


}