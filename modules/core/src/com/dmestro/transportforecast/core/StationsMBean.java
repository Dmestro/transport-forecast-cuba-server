package com.dmestro.transportforecast.core;

import com.dmestro.transportforecast.entity.Station;
import org.springframework.jmx.export.annotation.*;

import java.util.List;

@ManagedResource(description = "Station Loader")
public interface StationsMBean {
    @ManagedOperation(description = "Load stations form tosamara API")
    void loadStations();
}
