package com.dmestro.transportforecast.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Column;
import com.haulmont.cuba.core.entity.StandardEntity;
import com.haulmont.chile.core.annotations.NamePattern;

@NamePattern("%s|title")
@Table(name = "TRANSPORTFORECAST_STATION")
@Entity(name = "transportforecast$Station")
public class Station extends StandardEntity {
    private static final long serialVersionUID = 6393362668416447116L;

    @Column(name = "KS_ID", nullable = false)
    protected Integer ksId;

    @Column(name = "TITLE", nullable = false)
    protected String title;

    @Column(name = "LATITUDE")
    protected Double latitude;

    @Column(name = "LONGITUDE")
    protected Double longitude;

    public void setKsId(Integer ksId) {
        this.ksId = ksId;
    }

    public Integer getKsId() {
        return ksId;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    @Override
    public String toString() {
        return "Station{" +
                "ksId=" + ksId +
                ", title='" + title + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}