create table TRANSPORTFORECAST_STATION (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    KS_ID integer not null,
    TITLE varchar(255) not null,
    LATITUDE double precision,
    LONGITUDE double precision,
    --
    primary key (ID)
);
