-- begin TRANSPORTFORECAST_STATION
create table TRANSPORTFORECAST_STATION (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    KS_ID integer not null,
    TITLE varchar(255) not null,
    LATITUDE double precision,
    LONGITUDE double precision,
    --
    primary key (ID)
)^
-- end TRANSPORTFORECAST_STATION
-- begin TRANSPORTFORECAST_TRANSPORT
create table TRANSPORTFORECAST_TRANSPORT (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    KR_ID integer not null,
    NUMBER_ varchar(10) not null,
    TYPE_ varchar(255),
    ARRIVAL_TIME timestamp not null,
    STATION_ID uuid not null,
    TIME_TO_STOP integer,
    REMAINING_LENGTH double precision,
    --
    primary key (ID)
)^
-- end TRANSPORTFORECAST_TRANSPORT
-- begin TRANSPORTFORECAST_MESSAGE
create table TRANSPORTFORECAST_MESSAGE (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    TEXT varchar(255) not null,
    RATING integer not null,
    TRANSPORT_ID uuid not null,
    CREATION_TIME timestamp not null,
    --
    primary key (ID)
)^
-- end TRANSPORTFORECAST_MESSAGE
