-- begin TRANSPORTFORECAST_TRANSPORT
alter table TRANSPORTFORECAST_TRANSPORT add constraint FK_TRANSPORTFORECAST_TRANSPORT_STATION1 foreign key (STATION_ID) references TRANSPORTFORECAST_STATION(ID)^
create index IDX_TRANSPORTFORECAST_TRANSPORT_STATION on TRANSPORTFORECAST_TRANSPORT (STATION_ID)^
-- end TRANSPORTFORECAST_TRANSPORT
-- begin TRANSPORTFORECAST_MESSAGE
alter table TRANSPORTFORECAST_MESSAGE add constraint FK_TRANSPORTFORECAST_MESSAGE_TRANSPORT foreign key (TRANSPORT_ID) references TRANSPORTFORECAST_TRANSPORT(ID)^
create index IDX_TRANSPORTFORECAST_MESSAGE_TRANSPORT on TRANSPORTFORECAST_MESSAGE (TRANSPORT_ID)^
-- end TRANSPORTFORECAST_MESSAGE
