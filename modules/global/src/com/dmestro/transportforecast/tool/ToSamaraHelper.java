package com.dmestro.transportforecast.tool;

import com.dmestro.transportforecast.entity.Station;
import com.dmestro.transportforecast.entity.Transport;
import com.dmestro.transportforecast.tool.SHA1Calculator;
import com.haulmont.cuba.core.global.Metadata;
import com.haulmont.cuba.core.sys.MetadataImpl;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

/**
 * Created by Lytki on 26.12.2016.
 */
@Component
public class ToSamaraHelper {

    private static String STATIONS_URL = "http://tosamara.ru/api/classifiers/stopsFullDB.xml";
    private static String ROUTERS_AND_STOPS_CORRESPONDENCE_URL = "http://www.tosamara.ru/api/classifiers/routesAndStopsCorrespondence.xml";
    private static String API_URL = "http://tosamara.ru/api/json";
    private static String login = "LytkinD";
    private static String password = "pbS4CL";
    public ToSamaraHelper() {
    }

    @Inject
    private Metadata metadata;


    public List<Station> getStations() {
        List<Station> stations=null;
        try {

            URL stationsListUrl = new URL(STATIONS_URL);
            HttpURLConnection connection = (HttpURLConnection)stationsListUrl.openConnection();
            InputStream inputStream = new BufferedInputStream(connection.getInputStream());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(inputStream);
            connection.disconnect();

            NodeList nList = doc.getElementsByTagName("stop");
            stations = new ArrayList<>();
            for(int i = 0; i<nList.getLength();i++){
                Node node = nList.item(i);

//                Station station = new Station();
                Station station = metadata.create(Station.class);

                Element element = (Element)node;
                station.setTitle(getValueByTagName("title", element));
                station.setKsId(Integer.parseInt(getValueByTagName("KS_ID", element)));
                station.setLatitude(Double.parseDouble(getValueByTagName("latitude",element)));
                station.setLongitude(Double.parseDouble(getValueByTagName("longitude",element)));
//                if(getValueByTagName("busesCommercial",element)!=null)
//                    station.setBusesCommercial(new ArrayList<String>(Arrays.asList(getValueByTagName("busesCommercial",element).split(", "))));
//                if(getValueByTagName("busesMunicipal",element)!=null)
//                    station.setBusesMunicipal(new ArrayList<String>(Arrays.asList(getValueByTagName("busesMunicipal",element).split(", "))));
//                if(getValueByTagName("busesSeason",element)!=null)
//                    station.setBusesSeason(new ArrayList<String>(Arrays.asList(getValueByTagName("busesSeason",element).split(", "))));
//                if(getValueByTagName("busesSpecial",element)!=null)
//                    station.setBusesSpecial(new ArrayList<String>(Arrays.asList(getValueByTagName("busesSpecial",element).split(", "))));
//                if(getValueByTagName("trams",element)!=null)
//                    station.setTrams(new ArrayList<String>(Arrays.asList(getValueByTagName("trams",element).split(", "))));
//                if(getValueByTagName("trolleybuses",element)!=null)
//                    station.setTrolleybuses(new ArrayList<String>(Arrays.asList(getValueByTagName("trolleybuses",element).split(", "))));
//                if(getValueByTagName("metros",element)!=null)station.setMetros(new ArrayList<String>(Arrays.asList(getValueByTagName("metros",element).split(", "))));
                stations.add(station);
                //TODO: parse other information

            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        } catch (SAXException e) {
            e.printStackTrace();
        }
        return stations;
    }

    private String getValueByTagName(String tag, Element element) {
        NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = nodeList.item(0);
        return node==null?null:node.getNodeValue();
    }

    public List getFirstArrivalToStop(Station station, int count){
        try {
            // ?method=getFirstArrivalToStop&KS_ID=9,19,29,39&COUNT=10&os=android&clientid=appName%2F1.0&authkey=b1b3773a05c0ed0176787a4f1574ff0075f7521e
            StringBuilder stringBuilder = new StringBuilder(API_URL);
            stringBuilder.append("?method=getFirstArrivalToStop&KS_ID=")
                    .append(station.getKsId())
                    .append("&COUNT=")
                    .append(count)
                    .append("&os=android&clientid=")
                    .append(login)
                    .append("&authkey=")
                    .append(SHA1Calculator.calculate(station.getKsId().toString()+count+password));
            URL url = new URL(stringBuilder.toString());
            HttpURLConnection connection = (HttpURLConnection)url.openConnection();
            connection.setRequestMethod("GET");

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(connection.getInputStream()));
            String responseStr;
            StringBuffer response = new StringBuffer();

            while ((responseStr = in.readLine()) != null) {
                response.append(responseStr);
            }
            in.close();
            connection.disconnect();

            List transports = new ArrayList<Transport>();
            responseStr = response.toString();
            JSONObject responseJSON = new JSONObject(responseStr);
            JSONArray jsonArray = responseJSON.getJSONArray("arrival");
            for(int i=0;i<jsonArray.length();i++){
                JSONObject transportJSON = jsonArray.getJSONObject(i);
//                Transport transport = new Transport(); //metadata
                Transport transport = metadata.create(Transport.class); //metadata
                transport.setKrId(Integer.parseInt(transportJSON.getString("KR_ID")));
                transport.setNumber(transportJSON.getString("number"));
                transport.setTimeToStop(Integer.parseInt(transportJSON.getString("time")));
                transport.setType(transportJSON.getString("type"));
                transport.setRemainingLength(transportJSON.getDouble("remainingLength"));
                transport.setStation(station);
                transports.add(transport);
            }
            return transports;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }



}
