create table TRANSPORTFORECAST_MESSAGE (
    ID uuid,
    VERSION integer not null,
    CREATE_TS timestamp,
    CREATED_BY varchar(50),
    UPDATE_TS timestamp,
    UPDATED_BY varchar(50),
    DELETE_TS timestamp,
    DELETED_BY varchar(50),
    --
    TEXT varchar(255) not null,
    RATING integer not null,
    TRANSPORT_ID uuid not null,
    CREATION_TIME timestamp not null,
    --
    primary key (ID)
);
