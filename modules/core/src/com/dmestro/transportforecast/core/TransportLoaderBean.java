package com.dmestro.transportforecast.core;

import com.dmestro.transportforecast.entity.Station;
import com.dmestro.transportforecast.entity.Transport;
import com.dmestro.transportforecast.tool.ToSamaraHelper;
import com.haulmont.cuba.core.Persistence;
import com.haulmont.cuba.core.Transaction;
import com.haulmont.cuba.security.app.Authenticated;
import com.haulmont.cuba.security.app.Authentication;
import org.springframework.stereotype.Component;

import javax.inject.Inject;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

@Component(TransportLoaderBean.NAME)
public class TransportLoaderBean implements TransportLoader {

    public static final String NAME ="transportforecast_StationLoader";
    @Inject
    private ToSamaraHelper toSamaraHelper;
    @Inject
    protected Persistence persistence;

    @Inject
    Authentication authentication;

//    @Authenticated
    @Override
    public void load() {

        authentication.begin();
        try {

            List<Station> stations = new ArrayList<>(5);

            List<UUID> ids = new ArrayList<>(5);
            ids.add(UUID.fromString("2216a56c-02de-ee32-e8f8-419db2d3d631"));
            ids.add(UUID.fromString("23877f0d-e579-14b9-6aa1-6532f600e294"));
            ids.add(UUID.fromString("29af2bcc-0457-3950-1aaf-d0b9056188ee"));
            ids.add(UUID.fromString("2d646406-4115-b6e4-8f5b-921b72bd4206"));
            ids.add(UUID.fromString("2f96ead6-bcd9-0f6f-4498-fa4ac54fdfb9"));

            for(UUID id:ids){
                    stations.add(
                            persistence.createTransaction().execute( em -> {
                                return em.find(Station.class,id);
                            }
                        )
                    );
            }

            for (Station station : stations) {
                List<Transport> transports = toSamaraHelper.getFirstArrivalToStop(station, 10);
                for (Transport transport : transports) {
                    if (transport.getTimeToStop() <= 1 && transport.getRemainingLength()<=50) {
                        try(Transaction transaction = persistence.createTransaction()) {
                            Date date = new Date();
                            transport.setArrivalTime(date);

                            persistence.getEntityManager().persist(transport);
                            transaction.commit();
                            transaction.close();
                        }
                    }
                }
            }
        }finally {
            authentication.end();
        }

    }
}
